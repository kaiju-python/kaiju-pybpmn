from collections import OrderedDict
import asyncio
import os
import pickle
import sys

sys.path.insert(0, ".")

import logging.config

logger = logging.getLogger(__name__)

import time

from pybpmn.bpmn_process import BpmnProcess
from pybpmn.bpmn_parser import load
from pybpmn.user_task import UserTask

path = os.path.dirname(os.path.abspath(__file__))


class Handler:

    def __init__(self, processor):
        self.processor = processor

    def current_task_name(self, kargs):
        # принтуем название текущей задачи
        tasks = kargs.get('payload', {}).keys()
        current_task = ""
        for t in tasks:
            if t:
                current_task = t.upper()

        return current_task

    def on_enter_task(self, **kargs):
        logger.info("Entering task")
        current_task = self.current_task_name(kargs)
        print(f" -> Вход в Задачу {current_task}: ", kargs)

    def on_exit_task(self, **kargs):
        logger.info("Exiting task")
        current_task = self.current_task_name(kargs)
        print(f"<-Выход из задачи {current_task}: ", kargs['context'])

    def on_enter_task_2(self, **kargs):
        logger.info("Entering task task_2")
        current_task = self.current_task_name(kargs)

        print(f" -> Вход в Задачу {current_task}: ", kargs)

    def on_task_2(self, **kargs):
        context = kargs.get("context")
        payload = kargs.get("payload")
        payload["user"] = True
        task_context = kargs.get("task")
        task_context.update({
            "name1": "value1"
        })
        logger.info("Process task task_2")
        current_task = self.current_task_name(kargs)
        print(f" -  Обрабатываем задачу {current_task}: ", kargs)

    def on_exit_task_2(self, **kargs):
        logger.info("Exiting task task_2")
        current_task = self.current_task_name(kargs)
        print(f" <-  Выход из задачи {current_task}: ", kargs)

    def on_enter_task_3(self, **kargs):
        logger.info("Entering task task_3")
        current_task = self.current_task_name(kargs)
        print(f"-> Вход в Задачу {current_task}: ", kargs)

    def on_task_3(self, **kargs):
        logger.info("Process task task_3")
        current_task = self.current_task_name(kargs)
        print(f" -  Обрабатываем задачу {current_task}: ", kargs)

    def on_exit_task_3(self, **kargs):
        logger.info("Exiting task task_3")
        current_task = self.current_task_name(kargs)
        print(f" <-  Выход из задачи {current_task}: ", kargs)

    def on_enter_task_4(self, **kargs):
        logger.info("Entering task task_4")
        current_task = self.current_task_name(kargs)
        print(f"-> Вход в задачу {current_task}: ", kargs)

    def on_task_4(self, **kargs):
        time.sleep(3)
        logger.info("Process task task_4")
        current_task = self.current_task_name(kargs)

        print(f" -  Обрабатываем задачу {current_task}: ", kargs)

    def on_exit_task_4(self, **kargs):
        logger.info("Exiting task task_4")
        current_task = self.current_task_name(kargs)

        print(f" <-  Выход из задачи {current_task}: ", kargs)

    def on_enter_task_5(self, **kargs):
        logger.info("Entering task task_5")
        current_task = self.current_task_name(kargs)

        print(f"-> Вход в задачу {current_task}: ", kargs)

    def on_task_5(self, **kargs):
        time.sleep(5)
        context = kargs.get("context")
        context["user"] = True
        task_context = kargs.get("task")
        task_context.update({
            "name1": "value1"
        })
        logger.info("Process task task_5")
        current_task = self.current_task_name(kargs)

        print(f" -  Обрабатываем задачу {current_task}: ", kargs)

    def on_exit_task_5(self, **kargs):
        logger.info("Exiting task task_5")
        current_task = self.current_task_name(kargs)

        print(f" <-  Выход из задачи {current_task}: ", kargs)

    def on_enter_task_6(self, **kargs):
        logger.info("Entering task task_6")
        print(" -> Вход в задачу task_6:", kargs)

    def on_exit_task_6(self, **kargs):
        logger.info("Exiting task task_6")
        print(" <-  Выход из задачи task_6: ", kargs)


async def instance_from_file(save_filename, tasks):
    with open(save_filename, 'rb') as f:
        _data = pickle.load(f)
        last_task_name = _data['task_name']
        instance = _data['instance']
        is_complete = _data['complete']
        if not is_complete:
            start = False
            for task_name, args in tasks.items():
                if task_name == last_task_name:
                    start = True

                if start:
                    _task = instance.get_activity_by_name(task_name, restart=True)
                    if _task is not None:
                        await _task.complete(args)
                    else:
                        print(task_name, 'if _task is not None:', args)


async def bpmn_process(filename, tasks, save_filename):
    print("FILE NAME", filename)
    schema = load(open(filename, "r").read())
    from pprint import pprint
    pprint(schema)

    execute = True
    activities = []

    instance = BpmnProcess(schema)
    state = await instance.execute()
    activities = state.get("activities", [])
    while activities:
        _state = activities.pop()

        instance = BpmnProcess(schema)
        state = await instance.execute(_state)
        activities = state.get("activities", []) or []
        activities.extend(activities)
        print("\nNEXT>>", state)
        print("\n")
        execute = False

    # for task_name, args in tasks.items():
    #     _task = instance.get_activity_by_name(task_name)
    #     try:
    #         if _task is not None and isinstance(_task, UserTask):
    #             await _task.complete(args)
    #
    #         pickle.dump(
    #             {
    #                 'instance': instance,
    #                 'task_name': task_name,
    #                 'complete': True
    #             },
    #             open(save_filename, "wb")
    #         )
    #
    #     except Exception as error:
    #         print(f'Ошибка {error} в задаче: {task_name}')
    #         # FIXME: Заменить на БД.
    #         #  Для тестов, при ошибке сохраняем в pickle-файл c информацией о задаче на которой упала.
    #         pickle.dump(
    #             {
    #                 'instance': instance,
    #                 'task_name': task_name,
    #                 'complete': False
    #             },
    #             open(save_filename, "wb")
    #         )
    #
    #         # обрываем дальнейшее выполнение задачи, т.к. Exception
    #         break


async def async_bpmn_process(filename, save_filename, tasks):
    # await bpmn_process(filename, tasks, save_filename)
    # if os.path.exists(save_filename):
    #     await instance_from_file(save_filename, tasks)
    # else:
    await bpmn_process(filename, tasks, save_filename)


async def main():
    filename = f"{path}/data/diagram-4.bpmn.err"
    save_filename = f"{path}/data/diagram-4.bpmn.p1"

    tasks = OrderedDict({
        "Заполнить описание": {
            "users": "Valuetoadd", "attrs": (1, 2, 3)
        },
        "заполнить характерисики": {
            "user_group": "Valuetoadd",
            "attributes": "attributes",
        },
    })
    task = asyncio.create_task(
        async_bpmn_process(filename, save_filename, tasks)
    )
    await task


asyncio.run(main())
