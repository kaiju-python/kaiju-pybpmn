import xmltodict


class BpmnParser:
    @staticmethod
    def get_target_flow_by_id(seq_flows, seq_flow_id):
        for flow_dict in seq_flows:
            if flow_dict.get("@id") == seq_flow_id:
                return flow_dict.get("@targetRef")


def load(process_definition):
    json_process_definition = xmltodict.parse(process_definition, dict_constructor=dict)
    definitions = json_process_definition.get("bpmn:definitions", json_process_definition.get('definitions', {}))
    return definitions.get("bpmn:process", definitions.get('process', {})) or {}
