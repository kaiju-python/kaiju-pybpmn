import abc

from .task import Task


class Gateway(Task):

    @abc.abstractmethod
    async def execute(self, context, payload):
        pass
