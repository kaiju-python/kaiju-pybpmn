import asyncio
import concurrent.futures
import logging
import uuid
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime

from .end_event import EndEvent
from .exclusive_gateway import ExclusiveGateway
from .inclusive_gateway import InclusiveGateway
from .parallel_gateway import ParallelGateway
from .service_task import ServiceTask
from .start_event import StartEvent
from .task import Task
from .user_task import UserTask

logger = logging.getLogger(__name__)

activity_map = {
    "bpmn:serviceTask": ServiceTask,
    "bpmn:userTask": UserTask,
    "bpmn:exclusiveGateway": ExclusiveGateway,
    "bpmn:inclusiveGateway": InclusiveGateway,
    "bpmn:parallelGatewayy": ParallelGateway,
    "bpmn:endEvent": EndEvent,
    "serviceTask": ServiceTask,
    "userTask": UserTask,
    "exclusiveGateway": ExclusiveGateway,
    "inclusiveGateway": InclusiveGateway,
    "parallelGatewayy": ParallelGateway,
    "endEvent": EndEvent,


}

class BpmnProcess:
    handler = None

    def __init__(self, process_definition, handler=None, context=None):
        self.activities = []
        self.process_definition = process_definition
        self.process_instance_id = str(uuid.uuid4())
        self.context = {
            "process_instance_id": self.process_instance_id,
            "created_time": datetime.now()
        }
        self.payload = context or {}

        if handler:
            self.handler = handler(self)

    async def execute(self, state: dict = None):
        if not state:
            return await self._execute_start_event()
        elif type(state) is dict:
            activity_id = state["id"]
            activity_type = state["kind"]
            activity_data = self.get_activity_data(activity_type, activity_id)

            task = activity_map.get(activity_type)

            if task:
                _task = task(activity_data, self)
                return await _task.execute(self.context, self.payload)

    def get_activity_data(self, kind, _id):
        activity_type_data = self.process_definition.get(kind)

        if activity_type_data:
            if type(activity_type_data) is dict:
                activity_type_data = [activity_type_data]

            for task in activity_type_data:
                if task["@id"] == _id:
                    return task

    async def start_process(self, state=None):
        # TODO LOAD STATE AND RUN STOPPED TASK
        await self._start_execution()

    # def set_payload(self, payload):
    #     self.payload = payload

    async def _start_execution(self):
        await self._execute_start_event()

    async def _execute_start_event(self):
        start_event = StartEvent(self.process_definition.get("bpmn:startEvent", {}), self)
        return await start_event.execute(self.context, self.payload)

    # async def evaluate_results(self, results):
    #     if results is None:
    #         return
    #     activities = []
    #     for result in results:
    #         (activity_type, activity_id, activity_data) = result
    #         activity = self.get_activity_for_outgoing(activity_type, activity_id, activity_data)
    #         if activity:
    #             activities.append(activity)
    #
    #     functions = []
    #
    #     for activity in activities:
    #         # def function1():
    #         #     activity.execute(self.context,self.payload)
    #         functions.append((activity.execute, self.context, self.payload))
    #
    #     if (len(activities) > 0):
    #         await self.execute_parallel(len(activities), functions)

    async def evaluate_results(self, results):
        print("\nevaluate_results:", results)
        if results == None:
            return
        activities = []
        for result in results:
            (activity_type, activity_id, activity_data) = result
            activity = self.get_activity_for_outgoing(activity_type, activity_id, activity_data)
            if activity:
                activities.append(activity)

        functions = []
        print("\n>activities:", activities)
        print("\n")
        for activity in activities:
            # def function1():
            #     activity.execute(self.context,self.payload)
            functions.append((activity.execute, self.context, self.payload))

        if (len(activities) > 0):
            await self.execute_parallel(len(activities), functions)

    def blocks(self, n):
        import time
        log = logging.getLogger('blocks({})'.format(n))
        log.info('running')
        time.sleep(0.1)
        log.info('done')
        return n ** 2

    async def run_blocking_tasks(self, executor, ):
        log = logging.getLogger('run_blocking_tasks')
        log.info('starting')

        log.info('creating executor tasks')
        loop = asyncio.get_event_loop()
        blocking_tasks = [
            loop.run_in_executor(executor, self.blocks, i)
            for i in range(6)
        ]
        log.info('waiting for executor tasks')
        completed, pending = await asyncio.wait(blocking_tasks)
        results = [t.result() for t in completed]
        log.info('results: {!r}'.format(results))

        log.info('exiting')

    def old_execute_parallel(self, workers, functions):
        # TODO: не используется;
        results = []
        futures = []
        with ThreadPoolExecutor(max_workers=workers) as executor:
            for function_callable in functions:
                futures.append(executor.submit(*function_callable))

            concurrent.futures.wait(futures)

            for future in futures:
                results.append(future.result())
        return results

    async def execute_parallel(self, workers, functions):
        """
        Альтернатива для concurrent.futures.ThreadPoolExecutor
        https://pymotw.com/3/asyncio/executors.html
        :param workers:
        :param functions:
        :return:
        """
        print("execute_parallel:", functions)

        for i, *_args in functions:
            await i(*_args)
        # loop = asyncio.get_event_loop()
        #
        # # Создаем ограниченный пул потоков.
        # executor = concurrent.futures.ThreadPoolExecutor(
        #     max_workers=workers,
        # )
        #
        # #  ThreadPoolExecutor запускает свои рабочие потоки, а затем вызывает каждую из
        # #  предоставленных функций один раз в потоке.
        # blocking_tasks = [
        #     loop.run_in_executor(executor, *function_callable, )
        #     for i, function_callable in enumerate(functions)
        # ]
        # # ожидание исполнения задач
        # completed, pending = await asyncio.wait(blocking_tasks)
        # results = []
        # for task in completed:
        #     result = await task.result()
        #     results.append(result)
        #
        # return results

    def get_activity_for_outgoing(self, activity_type, activity_id, activity_data):
        for activity in self.activities:
            if hasattr(activity, "id") and activity.id == activity_id:
                return activity

        activity = None
        task = activity_map.get(activity_type)

        if task:
            activity = task(activity_data, self)
            self.activities.append(activity)

        return activity

    def get_activity_started(self):
        activities = []
        for activity in self.activities:
            if isinstance(activity, Task) and self.context.get(activity.name, {}).get("status") == "STARTED":
                activities.append(activity)

        return activities

    # def _execute_task(self, task_name):
    #     getattr(self.handler, f'on_enter_{task_name}')(self)
    #     getattr(self.handler, f'on_{task_name}')()
    #     getattr(self.handler, f'on_exit_{task_name}')(self)
    #
    #     if self.process_vars.get("task_name", {}).get("status", None) == "COMPLETE":
    #         self._complete_task(task_name)

    def get_activity_by_name(self, name):
        for activity_name, activity_dict in self.context.items():
            if type(activity_dict) is dict and activity_dict.get("activity_id") is not None and activity_name == name:
                for activity in self.activities:
                    if hasattr(activity, 'activity_id') and activity.activity_id == activity_dict.get("activity_id"):
                        return activity

        return None

    def _get_start_task_name(self):
        return self.process_definition.get("bpmn:startEvent", {})
