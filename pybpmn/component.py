import abc


class BpmnComponent:

    @abc.abstractmethod
    async def execute(self, context: dict, payload: dict):
        pass
