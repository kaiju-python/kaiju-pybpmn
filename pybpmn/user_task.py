import logging
import inspect
from datetime import datetime

from .task import Task

logger = logging.getLogger(__name__)


class UserTask(Task):

    async def execute(self, context, payload):
        print("UserTask.execute")
        self._execute(context, payload)

        name = self.name

        task = self.task_context
        task["status"] = "STARTED"

        payload_task = self.payload_task

        print("\nUserTask.name:", name)
        print("\n")

        if self.process_instance.handler is not None:
            on_enter_task = getattr(self.process_instance.handler, f"on_enter_task", None)
            on_enter_func = getattr(self.process_instance.handler, f"on_enter_{name}", None)

            if on_enter_task:
                on_enter_task = on_enter_task(context=context, task=payload_task, payload=payload,
                                              process_instance=self.process_instance)
                if inspect.iscoroutine(on_enter_task):
                    await on_enter_task

            if on_enter_func:
                on_enter_func = on_enter_func(context=context, task=payload_task,
                                              payload=payload,
                                              process_instance=self.process_instance)
                if inspect.iscoroutine(on_enter_func):
                    await on_enter_func

        return await self.complete({})

    async def complete(self, input_task_context):
        print("UserTask.complete")

        name = self.name
        task = self.task_context
        context = self.context
        payload = self.payload
        payload_task = self.payload_task

        if self.process_instance.handler is not None:
            on_exit_func = getattr(self.process_instance.handler, f"on_exit_{name}", None)
            on_exit_task = getattr(self.process_instance.handler, f"on_exit_task", None)

            if on_exit_func:
                on_exit_func = on_exit_func(context=context, task=payload_task,
                                            payload=payload,
                                            process_instance=self.process_instance)
                if inspect.iscoroutine(on_exit_func):
                    await on_exit_func

            if on_exit_task:
                on_exit_task = on_exit_task(context=context, task=payload_task,
                                            payload=payload,
                                            process_instance=self.process_instance)
                if inspect.iscoroutine(on_exit_func):
                    await on_exit_task

        task["end_time"] = datetime.now()
        task["status"] = "COMPLETED"
        task.update(input_task_context)

        return self.get_outgoing_activities()
        # await self.process_instance.evaluate_results(self.get_outgoing_activities())
