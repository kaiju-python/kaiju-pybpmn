import logging
from datetime import datetime

from .task import Task

logger = logging.getLogger(__name__)


class StartEvent(Task):

    async def execute(self, context, payload):
        self._execute(context, payload)

        context["status"] = "STARTED"

        self.task_context["start_time"] = datetime.now()
        self.task_context["end_time"] = datetime.now()

        result = self.get_outgoing_activities()
        # from pprint import pprint
        # pprint(activities)
        # print("\n")
        # await self.process_instance.evaluate_results(self.get_outgoing_activities())
        # print("END StartEvent")

        return result
